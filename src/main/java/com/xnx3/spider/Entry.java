package com.xnx3.spider;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import org.jvnet.substance.SubstanceLookAndFeel;
import org.jvnet.substance.skin.NebulaSkin;
import com.xnx3.util.AutoRun;

/**
 * 入口
 * @author 管雷鸣
 */
public class Entry {
	public static void main(String[] args) {
//		SubstanceOfficeBlue2007LookAndFeel s = new UI().UseLookAndFeelBySubstance();
//		s.setCurrentTheme(new SubstanceCremeTheme());
//		s.setCurrentBorderPainter(new StandardBorderPainter());
//		s.setCurrentWatermark(new SubstanceBubblesWatermark());
//		new SkinUtil().UseLookAndFeelBySubstance().setCurrentTheme(new SubstanceCremeTheme());
		
//		Global.mainUI.setVisible(true);
//		Global.mainUI.getTextArea_url().setText(Global.TEXTAREA_REMIND);
//		//隐藏更多设置，也就是设置 request 参数的面板
//		Global.mainUI.moreSetPanel_showAndHidden();
//		
//		Global.mainUI.setBounds(30, 50, 600, 400);
//		
//		AutoRun.versionCheck();//版本检测
//		

		JFrame.setDefaultLookAndFeelDecorated(true);
		JDialog.setDefaultLookAndFeelDecorated(true);
		SwingUtilities.invokeLater(new Runnable() { 
			public void run() {
//				SubstanceImageWatermark watermark = new SubstanceImageWatermark(MainEntry.class.getResourceAsStream("/res/icon.png"));
//				watermark.setKind(ImageWatermarkKind.SCREEN_CENTER_SCALE);
//				SubstanceSkin skin = new OfficeBlue2007Skin().withWatermark(watermark);   //初始化有水印的皮肤
				
				try{
					SubstanceLookAndFeel.setSkin(new NebulaSkin());
//					SubstanceLookAndFeel.setSkin(skin);
				}catch(Exception e){
					e.printStackTrace();
				}
				
				Global.mainUI.setVisible(true);
				Global.mainUI.getTextArea_url().setText(Global.TEXTAREA_REMIND);
				//隐藏更多设置，也就是设置 request 参数的面板
				Global.mainUI.moreSetPanel_use = true;
				Global.mainUI.moreSetPanel_showAndHidden();
				
				Global.mainUI.setBounds(30, 50, 600, 400);
				
				AutoRun.versionCheck();//版本检测
				
//				MainJframe mainJframe = new MainJframe();
//				mainJframe.setSize(425, 295);
//				mainJframe.setVisible(true);
				
//				new Tray();	//创建托盘
				
				//新版本检测
				//VersionCheck.check("http://version.xnx3.com/shopStorePCClient.html", Global.VERSION, "storeClient");
			}
		});
		
		
	}
}
